//
//  SurveyTableViewModel.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Foundation

struct SurveyTableViewModel {
    let backgroundImageUrl: URL?
    let title: String?
    let description: String?
    
    init(survey: Survey, highRes: Bool = true) {
        backgroundImageUrl = highRes ? URL(string: "\(survey.coverImageUrl?.absoluteString ?? "")l") : survey.coverImageUrl
        title = survey.title
        description = survey.description
    }
}
