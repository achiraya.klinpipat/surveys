//
//  SurveyTableViewCell.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import UIKit

final class SurveyTableViewCell: UITableViewCell {

    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var takeSurveyButton: UIButton!
    
    static let cellIdentifier = "SurveyTableViewCell"
    
    var viewModel: SurveyTableViewModel! {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func updateUI() {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        backgroundImageView.image(viewModel.backgroundImageUrl)
    }
    
    @IBAction private func takeSurveyButtonDidTouch(_ sender: Any) {
        
    }
}
