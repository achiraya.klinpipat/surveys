//
//  SurveyListViewModel.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Foundation

private let pageSize = 10

enum SurveyListViewType: Int, CaseIterable {
    case survey, loadMore
}

enum SurveyListActionType {
    case refresh, loadMore
}

final class SurveyListViewModel {
    var surveysList = [Survey]()
    
    var surveysDataSuccess: (() -> Void)?
    var surveysDataFailure: ((Error) -> Void)?
    
    private var pageNumber = 1
    private var canLoadMore = true
    private var isLoading = false
    
    init() {
        loadSurveys()
    }
    
    func performAction(action: SurveyListActionType) {
        switch action {
        case .refresh:
            resetState()
            loadSurveys()
        case .loadMore:
            loadSurveys()
        }
    }
    
    func numberOfSections() -> Int {
        return SurveyListViewType.allCases.count
    }
    
    func numberOfRows(_ section: Int) -> Int {
        switch typeForIndexPath(IndexPath(row: 0, section: section)) {
        case .survey:
            return surveysList.count
        case .loadMore:
            return canLoadMore && !surveysList.isEmpty && !isLoading ? 1 : 0
        }
    }
    
    func typeForIndexPath(_ indexPath: IndexPath) -> SurveyListViewType {
        return SurveyListViewType(rawValue: indexPath.section)!
    }
    
    func cellViewModelFor(_ indexPath: IndexPath) -> SurveyTableViewModel {
        return SurveyTableViewModel(survey: surveysList[indexPath.row])
    }
    
    private func resetState() {
        surveysList.removeAll()
        pageNumber = 1
        canLoadMore = true
    }
    
    private func loadSurveys() {
        isLoading = true
        SurveysService.surveysPaging(page: pageNumber, limit: pageSize).request(onSuccesss: { [weak self] (surveys: [Survey]) in
            guard let `self` = self else { return }
            self.surveysList = self.pageNumber == 0 ? surveys : self.surveysList + surveys
            self.pageNumber += 1
            self.canLoadMore = !surveys.isEmpty
            self.isLoading = false
            self.surveysDataSuccess?()
        }) { [weak self] error in
            self?.surveysDataFailure?(error)
        }
    }
}
