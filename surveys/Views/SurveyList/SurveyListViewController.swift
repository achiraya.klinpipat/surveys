//
//  SurveyListViewController.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import UIKit

private let loadingCellIdentifier = "LoadingCell"

final class SurveyListViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var takeSurveyButton: UIButton!
    
    var viewModel = SurveyListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindViewModelResponse()
        
        if UserDefaults.token == nil { updateCredentials() }
    }
    
    private func updateCredentials() {
        AuthService.requestToken.request(onSuccesss: { [weak self] (creds: AuthenCredentials) in
            UserDefaults.updateCredentials(creds)
            self?.viewModel = SurveyListViewModel()
        }) { [weak self] error in
            self?.showToast(message: error.localizedDescription)
        }
    }
    
    private func setupUI() {
        navigationItem.title = "SURVEYS"
        
        // setup left bar button item
        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "refresh_ic"), style: .plain, target: self, action: #selector(refreshSurveysDidTouch))
        leftBarItem.tintColor = .white
        navigationItem.leftBarButtonItem = leftBarItem
        
        // setup right bar button item
        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu_ic"), style: .plain, target: self, action: #selector(menuDidTouch))
        rightBarItem.tintColor = .white
        navigationItem.rightBarButtonItem = rightBarItem
        
        // setup vertical page control
        pageControl.transform = pageControl.transform.rotated(by: .pi / 2)
        
        tableView.estimatedRowHeight = tableView.bounds.height
        tableView.register(UINib(nibName: SurveyTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: SurveyTableViewCell.cellIdentifier)
    }
    
    private func bindViewModelResponse() {
        viewModel.surveysDataSuccess = { [weak self] in
            guard let `self` = self else { return }
            UIView.transition(with: self.view, duration: 0.5, options: [.transitionCrossDissolve], animations: { [weak self] in
                self?.updateUI()
            })
        }
        
        viewModel.surveysDataFailure = { [weak self] error in
            self?.showToast(message: error.localizedDescription)
        }
    }
    
    private func updateUI() {
        pageControl.numberOfPages = viewModel.numberOfRows(0)
        pageControl.isHidden = false
        tableView.reloadData()
        takeSurveyButton.isHidden = false
    }
    
    @objc private func refreshSurveysDidTouch() {
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.tableView.contentOffset = .zero
        }) { [weak self] _ in
            self?.pageControl.currentPage = 0
            self?.viewModel.performAction(action: .refresh)
        }
    }
    
    @objc private func menuDidTouch() {
        print("menu did touch")
    }
    
    @IBAction private func takeSurveyDidTouch(_ sender: Any) {
        navigationController?.pushViewController(UIViewController(), animated: true)
    }
}

// MARK: TableView Delegate & Datasource
extension SurveyListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch viewModel.typeForIndexPath(indexPath) {
        case .survey:
            return tableView.bounds.height
        case .loadMore:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.typeForIndexPath(indexPath) {
        case .survey:
            let cell = tableView.dequeueReusableCell(withIdentifier: SurveyTableViewCell.cellIdentifier, for: indexPath) as! SurveyTableViewCell
            cell.viewModel = viewModel.cellViewModelFor(indexPath)
            return cell
        case .loadMore:
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier, for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard viewModel.typeForIndexPath(indexPath) == .loadMore else { return }
        viewModel.performAction(action: .loadMore)
    }
}

// MARK: ScrollViewDelegate
extension SurveyListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(round(scrollView.contentOffset.y / scrollView.frame.size.height))
    }
}
