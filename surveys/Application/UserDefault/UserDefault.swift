//
//  UserDefault.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 14/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Foundation

enum UserDefaultKey: String {
    case token
    case credentials
}

extension UserDefaults {
    static var token: String? {
        return UserDefaults.standard.object(forKey: UserDefaultKey.token.rawValue) as? String
    }
    
    static var credentials: AuthenCredentials? {
        return UserDefaults.standard.object(forKey: UserDefaultKey.credentials.rawValue) as? AuthenCredentials
    }
    
    static func updateCredentials(token: String?) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: UserDefaultKey.token.rawValue)
        defaults.synchronize()
    }
    
    static func updateCredentials(_ creds: AuthenCredentials) {
        updateCredentials(token: creds.accessToken)
    }
}
