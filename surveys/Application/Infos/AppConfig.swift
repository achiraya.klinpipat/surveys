//
//  AppConfig.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 10/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Foundation

final class AppConfig {
    static let shared = AppConfig()
    private let configs: NSDictionary
    
    lazy var baseUrl = configStringValue(for: .baseUrl)
    lazy var accessToken = configStringValue(for: .accessToken)
    
    var token: String?
    
    init() {
        if let configPath = Bundle.main.path(forResource: .fileName, ofType: .fileType)
            , let loadedConfig = NSDictionary(contentsOfFile: configPath) {
            configs = loadedConfig
        } else {
            configs = [:]
        }
    }
    
    func configStringValue(for key: String, defaultValue: String = .empty) -> String {
        return configs.object(forKey: key) as? String ?? defaultValue
    }
    
    func updateToken(newToken: String) {
        token = newToken
    }
}

private extension String {
    static let baseUrl = "baseUrl"
    static let accessToken = "accessToken"
    static let fileName = "AppConfig"
    static let fileType = "plist"
    static let username = "username"
    static let password = "password"
    static let empty = ""
}
