//
//  SurveysAPIManager.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 13/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Moya
import Alamofire

public typealias Method = Alamofire.HTTPMethod

final class SurveysAPIManager {
    static let shared = SurveysAPIManager()
    
    let baseUrl = AppConfig.shared.baseUrl
    var provider = MoyaProvider<MultiTarget>()
}

extension TargetType {
    var baseURL: URL {
        return URL(string: SurveysAPIManager.shared.baseUrl) ?? URL(target: self)
    }
}

extension TargetType {
    @discardableResult
    func request<T: Codable>(provider: MoyaProvider<MultiTarget> = SurveysAPIManager.shared.provider,
                             onSuccesss: @escaping (T) -> Void,
                             onFailure: @escaping (Error) -> Void) -> Cancellable {
        return provider.request(.target(self)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let object = try decoder.decode(T.self, from: response.data)
                    onSuccesss(object)
                } catch {
                    do {
                        let object = try response.mapString()
                        if let object = object as? T {
                            onSuccesss(object)
                        }
                    } catch {
                        onFailure(error)
                    }
                }
            case .failure(let error):
                onFailure(error)
            }
        }
    }
}
