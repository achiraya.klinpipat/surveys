//
//  Authen.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Foundation

struct AuthenCredentials: Codable {
    let accessToken: String?
    let tokenType: String?
    let expiresIn: Int?
    let createdAt: Date?
}
