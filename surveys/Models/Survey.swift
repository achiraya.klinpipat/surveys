//
//  Survey.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Foundation

struct Survey: Codable {
    let id: String?
    let coverImageUrl: URL?
    let title: String?
    let description: String?
}
