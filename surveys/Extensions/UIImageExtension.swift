//
//  UIImageExtension.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 13/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Kingfisher

extension UIImageView {
    
    func image(_ url: URL?, placeholderImage: UIImage? = nil) {
        var kf = self.kf
        if let placeholderImage = placeholderImage {
            guard let loadingImageData = placeholderImage.jpegData(compressionQuality: 1.0) else { return }
            
            kf.indicatorType = .image(imageData: loadingImageData)
        }
        kf.setImage(with: url, options: [.transition(.fade(.timeInterval))])
    }
    
}

fileprivate extension Double {
    static let timeInterval = 0.2
}
