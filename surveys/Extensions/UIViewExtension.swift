//
//  UIViewExtension.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 9/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
            
}
