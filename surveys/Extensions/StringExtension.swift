//
//  StringExtension.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 10/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

extension String {
    static let headerKeyContentType = "Content-Type"
    static let headerKeyAccept = "Accept"
    static let headerKeyAuthorization = "Authorization"
    static let headerValueJSON = "application/json"
    static let headerLocalized = "Accept-Language"
}
