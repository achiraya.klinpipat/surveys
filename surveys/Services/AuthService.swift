//
//  AuthService.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 10/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Moya

enum AuthService {
    case requestToken
}

extension AuthService: TargetType {
    var baseURL: URL {
        let urlString = AppConfig.shared.baseUrl
        guard let url = URL(string: urlString) else {
            return URL.init(target: self)
        }
        return url
    }
    
    var path: String {
        return .authBasePath
    }
    
    var method: Method {
        return .post
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        let wrapRequestData: ([String : String]) -> Task = {
            body in
            do {
                let data = try JSONSerialization.data(
                    withJSONObject: body,
                    options: JSONSerialization.WritingOptions.prettyPrinted)
                return .requestData(data)
            } catch {
                return .requestData(Data())
            }
        }
        

        let body: [String : String] = [
            .grantType : .passwordKey,
            .userKey : AppConfig.shared.configStringValue(for: .userKey),
            .passwordKey : AppConfig.shared.configStringValue(for: .passwordKey)
        ]
        
        return wrapRequestData(body)
    }
    
    var headers: [String : String]? {
        let headers: [String : String] = [
            .headerKeyContentType : .headerValueJSON,
            .headerKeyAccept : .headerValueJSON,
        ]
        return headers
    }
}

extension AuthService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .none
    }
}

private extension String {
    static let authBasePath = "/oauth/token"
    static let grantType = "grant_type"
    static let userKey = "username"
    static let passwordKey = "password"
}
