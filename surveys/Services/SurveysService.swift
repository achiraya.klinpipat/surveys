//
//  SurveysService.swift
//  surveys
//
//  Created by Achiraya Klinpipat on 13/3/2562 BE.
//  Copyright © 2562 Achiraya. All rights reserved.
//

import Moya

enum SurveysService {
    case surveys
    case surveysPaging(page: Int, limit: Int)
}

extension SurveysService: TargetType {
    var baseURL: URL {
        let urlString = AppConfig.shared.baseUrl
        guard let url = URL(string: urlString) else {
            return URL.init(target: self)
        }
        return url
    }
    
    var path: String {
        switch self {
        case .surveys:
            return .surveysPath
        case .surveysPaging(let page, let limit):
            return String(format: .surveysWithPaginationPath, page, limit)
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        var headers: [String : String] = [
            .headerKeyContentType : .headerValueJSON,
            .headerKeyAccept : .headerValueJSON
        ]
        
        if let token = UserDefaults.token {
            headers[.headerKeyAuthorization] = String(format: .bearerFormat, authorizationType.value ?? "", token)
        }
        
        return headers
    }
}

extension SurveysService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .bearer
    }
}

private extension String {
    static let surveysPath = "/surveys.json"
    static let bearerFormat = "%@ %@"
    static let surveysWithPaginationPath = "/surveys.json?page=%d&per_page=%d"
}
